from flask import Flask, render_template, request
import chatbot

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/get")
def get_bot_response():
    userText = request.args.get('msg')
    bot_answer = chatbot.answer(userText)
    return (str(bot_answer))

if __name__ == "__main__":
    app.run()
